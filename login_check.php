<?php
	@session_start();

	$page = basename($_SERVER['PHP_SELF']);
	if($page != "login.php") {
		if(!isset($_SESSION['admin'])) {
			header("Location: login.php");
		}
	}
?>
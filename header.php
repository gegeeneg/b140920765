
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">


    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/screen.css" type="text/css" media="screen" title="default" />
<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
 
<!--  checkbox styling script -->
<script src="js/jquery/ui.core.js" type="text/javascript"></script>
<script src="js/jquery/ui.checkbox.js" type="text/javascript"></script>
<script src="js/jquery/jquery.bind.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('input').checkBox();
    $('#toggle-all').click(function(){
    $('#toggle-all').toggleClass('toggle-checked');
    $('#mainform input[type=checkbox]').checkBox('toggle');
    return false;
    });
});
</script>  


<![if !IE 7]>

<!--  styled select box script version 1 -->
<script src="js/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.styledselect').selectbox({ inputClass: "selectbox_styled" });
});
</script>
 

<![endif]>


<!--  styled select box script version 2 --> 
<script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
    $('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>

<!--  styled select box script version 3 --> 
<script src="js/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
});
</script>

<!--  styled file upload script --> 
<script src="js/jquery/jquery.filestyle.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(function() {
    $("input.file_1").filestyle({ 
    image: "images/forms/upload_file.gif",
    imageheight : 29,
    imagewidth : 78,
    width : 300
    });
});
</script>

<!-- Custom jquery scripts -->
<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>
 
<!-- Tooltips -->
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dimensions.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
    $('a.info-tooltip ').tooltip({
        track: true,
        delay: 0,
        fixPNG: true, 
        showURL: false,
        showBody: " - ",
        top: -35,
        left: 5
    });
});
</script> 

<!--  date picker script -->
<link rel="stylesheet" href="css/datePicker.css" type="text/css" />
<script src="js/jquery/date.js" type="text/javascript"></script>
<script src="js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
    .datePicker(
        // associate the link with a date picker
        {
            createButton:false,
            startDate:'01/01/2005',
            endDate:'31/12/2020'
        }
    ).bind(
        // when the link is clicked display the date picker
        'click',
        function()
        {
            updateSelects($(this).dpGetSelected()[0]);
            $(this).dpDisplay();
            return false;
        }
    ).bind(
        // when a date is selected update the SELECTs
        'dateSelected',
        function(e, selectedDate, $td, state)
        {
            updateSelects(selectedDate);
        }
    ).bind(
        'dpClosed',
        function(e, selected)
        {
            updateSelects(selected[0]);
        }
    );
    
var updateSelects = function (selectedDate)
{
    var selectedDate = new Date(selectedDate);
    $('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
    $('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
    $('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
    .bind(
        'change',
        function()
        {
            var d = new Date(
                        $('#y').val(),
                        $('#m').val()-1,
                        $('#d').val()
                    );
            $('#date-pick').dpSetSelected(d.asString());
        }
    );

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
</head>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

    <!-- start logo -->
    <div id="logo">
    <a href=""><img src="images/shared/logo.png" width="156" height="40" alt="" /></a>
    </div>
    <!-- end logo -->
    
    <!--  start top-search -->
    <div id="top-search">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><input type="text" value="Хайлт хийх" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" /></td>
        <td>
         
        <select  class="styledselect">
            <option value=""> Бүгдээс нь</option>
            <option value=""> Оюутнуудаас</option>
            <option value=""> Мэдээ мэдээллээс</option>
            <option value="">Тэтгэлгээс</option>
            <option value="">Ажлын зараас</option>
        </select> 
         
        </td>
        <td>
        <input type="image" src="images/shared/top_search_btn.gif"  />
        </td>
        </tr>
        </table>
    </div>
    <!--  end top-search -->
    <div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
    
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 

        <!-- start nav-right -->
        <div id="nav-right">
        
            <div class="nav-divider">&nbsp;</div>
            <div class="showhide-account"><img src="images/shared/nav/nav_myaccount.gif" width="93" height="14" alt="" /></div>
            <div class="nav-divider">&nbsp;</div>
            <a href="login.html"  id="logout"><img src="images/shared/nav/nav_logout.gif" width="64" height="14" alt="" /></a>
            <div class="clear">&nbsp;</div>
        
            <!--  start account-content --> 
            <div class="account-content">
            <div class="account-drop-inner">
                <a href="" id="acc-settings">Тохиргоо</a>
                <div class="clear">&nbsp;</div>
                <div class="acc-line">&nbsp;</div>
                <a href="" id="acc-details">Өөрийн мэдээлэл</a>
                <div class="clear">&nbsp;</div>
                <div class="acc-line">&nbsp;</div>
                <a href="" id="acc-settings">Нууц үг өөрчлөх</a>
                <div class="clear">&nbsp;</div>
                <div class="acc-line">&nbsp;</div>
                <a href="" id="acc-inbox">Ирсэн мейлүүд</a>
                <div class="clear">&nbsp;</div>
                <div class="acc-line">&nbsp;</div>
                <a href="" id="acc-stats">Үзүүлэлт</a> 
            </div>
            </div>
            <!--  end account-content -->
        
        </div>
        <!-- end nav-right -->


        <div class="nav">
        <div class="table">
        
        <ul class="select">
            <li>
                <a href="Student.php"><b>Нүүр хуудас</b></a>
               
            </li>
        </ul>
        
        <div class="nav-divider">&nbsp;</div>

        <ul class="select">
            <li><a href="studentAward.php"><b>Амжилт</b></a>
               
            </li>
        </ul>

        <div class="nav-divider">&nbsp;</div>
        <ul class="select">
            <li>
                <a href="News.php"><b>Мэдээ мэдээлэл</b></a>
                
            </li>
        </ul>
         <div class="nav-divider">&nbsp;</div>
        <ul class="select">
            <li>
                <a href="AddAds.php"><b>Нэмэх</b></a>
                
            </li>
        </ul>
        
        
        <div class="clear"></div>
        </div>
    
        <!--  start nav -->

</div>
<div class="clear"></div>
<!--  start nav-outer -->
</div>